#include <imgui.h>
#include <imgui-SFML.h>

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Network.hpp>

#include "student_simulator.h"

int main()
{
    auto student_ = new Student();
    char nameString_[2048] = "";
    std::string errorString_ = "";

    sf::RenderWindow window(sf::VideoMode(800, 480), "Arduino UDP");
    window.setFramerateLimit(60);
    if(ImGui::SFML::Init(window)) {
        sf::Clock deltaClock;
        while (window.isOpen()) {
            sf::Event event{};
            while (window.pollEvent(event)) {
                ImGui::SFML::ProcessEvent(window, event);

                if (event.type == sf::Event::Closed) {
                    window.close();
                }
            }
            ImGui::SFML::Update(window, deltaClock.restart());

//            app.display(window.getSize().x, window.getSize().y);

            ImGui::SetNextWindowPos(ImVec2(0, 0));
            ImGui::SetNextWindowSize(ImVec2(window.getSize().x, window.getSize().y));
            ImGui::Begin("Example", nullptr, ImGuiWindowFlags_::ImGuiWindowFlags_NoMove
                                             | ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse
                                             | ImGuiWindowFlags_::ImGuiWindowFlags_NoResize
                                             | ImGuiWindowFlags_::ImGuiWindowFlags_NoTitleBar);


//        First Column
            ImGui::Columns(3);

            ImGui::Text(std::string("Name => " + student_->Name()).c_str());
            ImGui::LabelText("", "==== CHARACTERISTICS ====");
            ImGui::Text(std::string("Health => " + std::to_string(student_->Health())).c_str());
            ImGui::Text(std::string("Energy => " + std::to_string(student_->Energy())).c_str());
            ImGui::Text(std::string("Sence => " + std::to_string(student_->Sense())).c_str());
            ImGui::Text(std::string("Emotion => " + std::to_string(student_->Emotion())).c_str());
            ImGui::Text(std::string("IQ => " + std::to_string(student_->Iq())).c_str());

//        Second Column
            ImGui::NextColumn();

            ImGui::InputText("Name", nameString_, 2048);
            if ( ImGui::Button("Change name") )
            {
                errorString_ = "";

                if( nameString_ != "")
                {
                    if(student_->is_alive())
                    {
                        std::string name = nameString_;
                        if(name=="")
                        {
                            errorString_ = "Enter student's name!";
                        }
                        else
                        {
                            student_->SetName(name);
                        }
                    }
                    else
                    {
                        errorString_ = "Your player lost a life resource. Push the button START GAME for new game";
                    }
                }
                else
                {
                    errorString_ = "Push the button START GAME";
                }
            }

//        Third Column
            ImGui::NextColumn();

            if ( ImGui::Button("Study") )
            {
                if(nameString_!="")
                {
                    if(student_->is_alive())
                    {
                        student_->study();
                    }
                    else
                    {
                        errorString_ = "Your player lost a life resource. Push the button START GAME for new game";
                    }
                }
                else
                {
                    errorString_ = "Push the button START GAME";
                }
            }
            if ( ImGui::Button("Have a party") )
            {
                if(nameString_!=""){
                    if(student_->is_alive()){
                        student_->sleep();
                    }
                    else{
                        errorString_ = "Your player lost a life resource. Push the button START GAME for new game";
                    }
                }
                else{
                    errorString_ = "Push the button START GAME";
                }
            }
            if ( ImGui::Button("Watch tik-tok") )
            {
                if(nameString_!="")
                {
                    if(student_->is_alive())
                    {
                        student_->have_party();
                    }
                    else{
                        errorString_ = "Your player lost a life resource. Push the button START GAME for new game";
                    }
                }
                else{
                    errorString_ = "Push the button START GAME";
                }
            }
            if ( ImGui::Button("Sleep") )
            {
                if(nameString_!="")
                {
                    if(student_->is_alive())
                    {
                        student_->eat();
                    }
                    else{
                        errorString_ = "Your player lost a life resource. Push the button START GAME for new game";
                    }
                }
                else
                {
                    errorString_ = "Push the button START GAME";
                }
            }
            if ( ImGui::Button("Eat") )
            {
                if(nameString_!="")
                {
                    if(student_->is_alive())
                    {
                        student_->watch_tv();
                    }
                    else
                    {
                        errorString_ = "Your player lost a life resource. Push the button START GAME for new game";
                    }
                }
                else
                {
                    errorString_ = "Push the button START GAME";
                }
            }

            ImGui::Columns(1);

            if ( ImGui::Button("Start game") )
            {
                student_ = new Student();
                student_->SetName("DEFAULT NAME");
            }

            ImGui::Text(errorString_.c_str());

            ImGui::End();

            window.clear();
            ImGui::SFML::Render(window);
            window.display();
        }
        ImGui::SFML::Shutdown();
    }

    delete student_;

    return 0;
}
