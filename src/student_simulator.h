//! @file student_simulator.h
//! @author Меньшикова В.А.
//! @note Ответственный: Полевой Д.В.
//! @brief Заголовочный фойл, описывающий класс Student

#ifndef STUDENT_SIMULATOR_H
#define STUDENT_SIMULATOR_H

#include <string>

class Student 
{
private:

	std::string name; //!< имя студента, т. е. действующего персонажа игры)

	int health; //!< состояние здоровья студента на данный момент из 100 очков
     
	int energy; //!< энергетический уровень студента на данный момент из 100 очков

	int iq; //!< состояние интеллектуальных способностей студента на данный момент из 100 очков    

	int sense;	//!< душевное состояние студента на данный момент из 100 очков     

	int emotion;  //!< эмоциональное состояние студента из 100 на данный момент

	//!@brief 		Изменение параметров студента
	//	@param[in]	hp - состояние здоровье
	//	@param[in]	en - энергетический уровень
	//	@param[in]	i - состояние интеллектуальных способностей
	//	@param[in]	sen - душевное состояние
	//	@param[in]	em - эмоциональное состояние
    void change_params(int hp, int en, int i, int sen, int em);

	//!@brief 		Изменение полей класса 
	//	@param[in]	field - название поля, значение которого необходимо поменять
	//	@param[in]	d - значение, на которое необходимо поменять данное поле
    void change_field(int& field, int d);

	//!@brief	Изменение полей класса при определенных значениях энергетического состояния, состояния здоровья и душевного состояния
	void effects();

	//!@brief
	void show();

public:
	//!@brief 		Конструктор класса Студент
	//	@param[in]	health - состояние здоровье
	//	@param[in]	energy - энергетический уровень
	//	@param[in]	iq - состояние интеллектуальных способностей
	//	@param[in]	sense - душевное состояние
	//	@param[in]	emotion - эмоциональное состояние
	Student(int health = 100, int energy = 100, int iq = 100, int sense = 100, int emotion = 100);

	//!@brief 		Изменения имя студента
	//	@param[in]	name - имя студента
    void SetName(std::string name);

	//!@brief 		Возвращение имя студента
    std::string Name();

	//!@brief 		Изменение состояния здоровья студента
    int Health();

	//!@brief		Изменения энергетического состояния студента
    int Energy();

	//!@brief		Изменения интеллектуальных способностей студента
    int Iq();

	//!@brief 		Изменение душевного состояния студента
    int Sense();

	//!@brief 		Изменение эмоционального состояния студента
    int Emotion();

	//!@brief		Изменение параметров при нажатии на кнопку “Eat”
    void eat();

	//!@brief		Изменение параметров при нажатии на кнопку “Have party”
    void have_party();

	//!@brief		Изменение параметров при нажатии на кнопку “Study” 
    void study();

	//!@brief		Изменение параметров при нажатии на кнопку “Sleep”
    void sleep();

	//!@brief 		Изменение параметров при нажатии на кнопку “Watch TV”
    void watch_tv();

	//!@brief		Проверка поля состояния здоровья студента
    bool is_alive();

};

#endif // STUDENT_SIMULATOR_H
