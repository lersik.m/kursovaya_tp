#include "student_simulator.h"

    void Student::change_params(int hp, int en, int i, int sen, int em) {
        change_field(health, hp);
        change_field(energy, en);
        change_field(iq, i);
        change_field(sense, sen);
        change_field(emotion, em);
    }
    void Student::change_field(int& field, int d) {
        if (field + d > 100) {
            field = 100;
        }
        else if (field + d < 0) {
            field = 0;
        }
        else {
            field = field + d;
        }
    }
    void Student::effects() {
        if (energy == 0) {
            change_field(health, -25);
        }
        else if (energy == 100) {
            change_field(health, -10);
        }
        if (sense <= 10) {
            change_field(health, -15);
        }
        if (emotion < 25) {
            change_field(sense, -5);
        }
    }

Student::Student(int health, int energy, int iq, int sense, int emotion) {
        this->health = health;
        this->energy = energy;
        this->iq = iq;
        this->sense = sense;
        this->emotion = emotion;
    }

    void Student::SetName(std::string name){
        this->name = name;
    };
    std::string Student::Name(){
        return name;
    }

    int Student::Health(){
        return health;
    }
    int Student::Energy(){
        return energy;
    }
    int Student::Iq(){
        return iq;
    }
    int Student::Sense(){
        return sense;
    }
    int Student::Emotion(){
        return emotion;
    }

    void Student:: eat() {
        if (is_alive()) {
            change_params(1, 7, -1, 0, -2);
            effects();
        }

    }
    void Student:: have_party() {
        if (is_alive()) {
            change_params(-3, -3, -3, 7, 6);
            effects();
        }
    }
    void Student:: study() {
        if (is_alive()) {
            change_params(-2, -4, 5, -5, -2);
            effects();
        }
    }
    void Student:: sleep() {
        if (is_alive()) {
            change_params(2, -2, 0, 7, 0);
            effects();
        }
    }
    void Student:: watch_tv() {
        if (is_alive()) {
            change_params(-2, -3, -3, 1, 5);
            effects();
        }
    }

    bool Student::is_alive() {
        if (health > 0) {
            return true;
        }
        else {
            return false;
        }
    }
